# Description
  Building a simple Blog Engine using ASP.NET Core

### Urls
  
  - Back-end: https://localhost:44383/
  - Front-end: http://localhost:8080/
  - Database: http://localhost:7474/

### Features      
  
  - For blog owner:
  - -Login/Logout
  - -Manage categories (Name, Description)
  - -Manage post (Slug, ShortDescription, Content, ThumbnailImage, CreatedDate, UpdatedDate)
  - -For internet users:
  - -Home page: lasted posts, categories menu
  - -View posts by category
  - -View post details

 
### Technology

Fontend:

* [vuecli] - single page app (-v3.0)!
* [VueFroala] - text editor
* [Bootstrap vue] - great UI boilerplate for modern web apps
* [vuesax] - design control, component, notify...
* [vee-validate] - validate form
* [Vuex] - two way binding mechanism
* [FontAwesomeIcon] - design icon..
* [jQuery] - wow

BackEnd:

* Asp.net core 2.1 + Api
* Jwt authentication token
* Identity neo4j

Database:

* neo4j  graph database
* setup database: local database, username: neo4j, password 123
