const baseUrl = process.env.VUE_APP_BASE_URL_API + '/post'
const token = localStorage.getItem('jwt_token')
export default {
  created (data) {
    console.log(token)
    return fetch(baseUrl + '/add', {
      method: 'post',
      dataType: 'application/json',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {})
  },
  getData () {
    return fetch(baseUrl + '', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  },
  getData () {
    return fetch(baseUrl + '', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  },
  getLastPost () {
    return fetch(baseUrl + '/getlastpost', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  },
  getDataByPostId (id) {
    return fetch(baseUrl + '/getbypostid/' + id, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  },
  getDetail (id) {
    return fetch(baseUrl + '/getdetail/' + id, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  },
  deleted (id) {
    return fetch(baseUrl + '/delete/' + id, {
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {})
  },
  edit (data) {
    return fetch(baseUrl + '/edit', {
      method: 'post',
      dataType: 'application/json',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {})
  }
}
