// const baseUrl = "http://192.168.194.207:8081/api/categories";
const baseUrl = process.env.VUE_APP_BASE_URL_API + '/categories'
const token = localStorage.getItem('jwt_token')
export default {
  created (data) {
    return fetch(baseUrl + '/add', {
      method: 'post',
      dataType: 'application/json',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json()
    })
  },
  getData () {
    debugger
    return fetch(baseUrl + '', {
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    }).then(res => {
      return res.json()
    })
  },
  deleted (id) {
    return fetch(baseUrl + '/delete/' + id, {
      method: 'get',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify()
    }).then(res => {
      return res.json()
    })
  },
  edit (data) {
    return fetch(baseUrl + '/edit', {
      method: 'post',
      dataType: 'application/json',
      headers: {
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    }).then(res => {
      return res.json()
    })
  }
}
