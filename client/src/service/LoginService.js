import axios from 'axios'
const baseUrl = process.env.VUE_APP_BASE_URL_API
export default {
  created (data) {
    return fetch(baseUrl + '/account', {
      method: 'post',
      dataType: 'json',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  },
  login (data) {
    return fetch(baseUrl + '/account/login', {
      method: 'post',
      dataType: 'json',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(data)
    })
      .then(res => {
        return res.json()
      })
      .catch(err => {
        return err.json()
      })
  }
}
