import Vue from 'vue'
import Router from 'vue-router'
import { constants } from 'os'
import store from './store.js'
import postDetail from '@/views/users/PostDetailView.vue'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/blog',
      name: 'blog',
      component: () => import('@/views/users/Layout.vue'),
      redirect: '/blog-home',
      children: [
        {
          path: '/blog-home',
          name: 'blog-home',
          component: () => import('@/views/users/HomeLayout.vue')
        },
        {
          path: '/blog-list/:url',
          name: 'blog-list',
          component: () => import('@/views/users/PostListView.vue')
        },
        {
          path: '/blog-detail/:url',
          name: 'blog-detail',
          component: postDetail
        }
      ]
    },
    {
      path: '/',
      name: 'root',
      redirect: '/blog'
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/shared/Login.vue')
    },
    {
      path: '/admin/categories',
      name: 'categories',
      component: () => import('./views/admin/Categories.vue')
    },
    {
      path: '/admin/post',
      name: 'post',
      component: () => import('@/views/admin/Post.vue')
    },
    {
      path: '/admin/postadd',
      name: 'addpost',
      component: () => import('@/components/admin/post/AddPost.vue')
    },
    {
      path: '/admin/postedit',
      name: 'editpost',
      component: () => import('@/components/admin/post/EditPost.vue')
    },
    {
      path: '/admin/demo',
      name: 'demo',
      component: () => import('@/components/admin/post/demo.vue')
    },
    {
      path: '/signin',
      name: 'signin',
      component: () => import('@/views/shared/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('@/views/shared/Register.vue')
    }
  ]
})
router.beforeEach((to, from, next) => {
  var token = localStorage.getItem('jwt_token')
  var role = localStorage.getItem('jwt_role')
  if (to.fullPath == '/home') {
    store.state.layout = 'user'
  } else if (to.fullPath == '/login') {
    store.state.layout = 'login'
  } else if (
    to.fullPath == '/admin/categories' ||
    to.fullPath == '/admin/post' ||
    to.fullPath == '/admin/postadd' ||
    to.fullPath == '/admin/postedit'
  ) {
    if (role == 'MANAGER' && token != '' && token != undefined) {
      store.state.layout = 'admin'
    } else {
      store.state.layout = 'login'
    }
  }
  next()
})
export default router
