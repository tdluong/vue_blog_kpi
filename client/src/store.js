import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    layout: 'user',
    component: 'categories',
    item_data_add: [],
    total_row_table: 0,
    is_add_popup: false,
    select_item_data: 1,
    categories_id: 0,
    categories_data: [],
    is_admin_role: false,
    last_post_item: []
  },
  mutations: {
    SET_LAYOUT (state, payload) {
      state.layout = payload
    },
    SET_COMPONENT (state, payload) {
      state.component = payload
    },
    getData (state, itemLoad) {
      state.item_data_add = itemLoad
    },
    totalRowData (state, itemTotal) {
      state.total_row_table = itemTotal.length
    },
    closeAddPopup (state, isStatus) {
      state.is_add_popup = isStatus
    },
    setSelectItem (state, data) {
      state.select_item_data = data
    },
    setCategoriesId (state, id) {
      state.categories_id = id
    },
    setCategoriesList (state, item) {
      state.categories_data = item
    },
    setRoleAdmin (state, role) {
      state.is_admin_role = role
    },
    setLastPost (state, item) {
      state.last_post_item = item
    }
  },
  actions: {},
  getters: {
    layout (state) {
      return state.layout
    },
    component (state) {
      return state.conponent
    }
  },
  setter: {}
})
export default store
