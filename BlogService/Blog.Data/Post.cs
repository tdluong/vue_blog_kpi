﻿using System;

namespace Blog.Data
{
    public class Post: BaseEntity
    {
        public string Id { get; set; }
        public string CategoriesId { get; set; }
        public string Slug { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
