﻿namespace Blog.Data
{
    public class DataResult
    {
        public string Message { get; set; }
        public bool IsError { get; set; }
        public object Item { get; set; }
    }
}
