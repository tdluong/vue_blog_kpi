﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class Categories: BaseEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
