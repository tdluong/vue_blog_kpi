﻿using Blog.Data;
using Blog.service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Blog.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : Controller
    {
        private readonly IPostService postService;
        public PostController(IPostService postService)
        {
            this.postService = postService;
        }

        [HttpGet()]
        public async Task<ActionResult<DataResult>> Get()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await postService.GetDataSync();
            
            return Ok(result);
        }
        [HttpGet("getdetail/{id}")]
        public async Task<ActionResult<DataResult>> GetDetail(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await postService.GetDetailsSync(id);
            return Ok(result);
        }
        [HttpGet("getlastpost")]
        public async Task<ActionResult<DataResult>> GetLastPost()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await postService.GetLastPostaSync();
            return Ok(result);
        }
        [HttpGet("getbypostid/{id}")]
        public async Task<ActionResult<DataResult>> GetPostsById(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await postService.GetDataByPostIdAsync(id);
            return Ok(result);
        }
        [HttpPost("add"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<DataResult>> Create([FromBody] Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await postService.CreateSync(post);
            return Ok(result);
        }

        [HttpGet("delete/{id}"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<DataResult>> Delete(string id)
        {
            if (!ModelState.IsValid)
            {

                return BadRequest(ModelState);
            }
            var result = await postService.DeleteSync(id);     
            return Ok(result);
        }

        [HttpPost("edit"), Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ActionResult<DataResult>> Edit([FromBody]Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await postService.EditSync(post);           
            return Ok(result);
        }
    }
}