﻿using AspNetCore.Identity.Neo4j;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Api.Uliti
{
    public class Roles
    {
        private readonly RoleManager<Neo4jIdentityRole> roleManager;
        public Roles(RoleManager<Neo4jIdentityRole> roleManager)
        {
           
        }
        public async  void Seed()
        {
            /* Create Roles */
            IEnumerable<Neo4jIdentityRole> allRoles = Enum.GetValues(typeof(Neo4jIdentityRole)).Cast<Neo4jIdentityRole>();

            foreach (Neo4jIdentityRole role in allRoles)
            {
                if (!await roleManager.RoleExistsAsync(role.ToString()))
                {
                    await roleManager.CreateAsync(new Neo4jIdentityRole
                    {
                        Name = role.ToString(),
                        NormalizedName = role.ToString().ToUpper()
                    });
                }
            }
        }
    }
}
