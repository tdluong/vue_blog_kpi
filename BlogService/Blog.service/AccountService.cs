﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blog.Data;
using Blog.Repo;
using Neo4j.Driver.V1;

namespace Blog.service
{
    public class AccountService : IAccountService
    {
        private IRepository<IdentityRole> acountReposity;
        public AccountService(IRepository<IdentityRole> acountReposity)
        {
            this.acountReposity = acountReposity;
        }
        public async Task<DataResult> AddToRoleAsync( string email)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "Match (u:User {Email: $email}), (r:Role {Name: 'Manager'}) "+
                    " Create (u)-[ir:InRole]->(r)";
                var obj = new
                {
                    email = email
                };
                 await acountReposity.CreateRelationAsync(statement, obj);
             
                return new DataResult { IsError = false, Message = "get role success!", Item = null };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }

        public async Task<DataResult> GetRoleByEmai(string email)
        {
            try
            {
                var id = Guid.NewGuid().ToString();
                string statement = "MATCH (u:User {Email: '"+ email + "'})-[ir:InRole]->(r:Role) RETURN r.Id as Id, r.Name as Name, r.NormalizedName as NormalizedName";
                var cursor = await acountReposity.GetRoleByUser(statement, new object());
                List<IdentityRole> lstRole = new List<IdentityRole>();
                while (await cursor.FetchAsync())
                {
                    lstRole.Add(new IdentityRole
                    {
                        Id = cursor.Current.Values["Id"].ToString(),
                        Name = cursor.Current.Values["Name"].ToString(),
                        NormalizedName = cursor.Current.Values["NormalizedName"].ToString()
                    });
                }
                return new DataResult { IsError = false, Message = "get role success!", Item = lstRole };
            }
            catch (Exception ex)
            {
                return new DataResult { IsError = true, Message = ex.Message };
            };
        }

        public Task<DataResult> GetUsersInRoleAsync()
        {
            throw new NotImplementedException();
        }
        private async Task<List<Categories>> GetIdentityListAsync(IStatementResultCursor cursor)
        {
            List<Categories> lstCategories = new List<Categories>();
            var i = 0;
            while (await cursor.FetchAsync())
            {
                i++;
                lstCategories.Add(new Categories
                {
                    OrderIndex = i,
                    Id = cursor.Current.Values["Id"].ToString(),
                    Name = cursor.Current.Values["Name"].ToString(),
                    Description = cursor.Current.Values["Description"].ToString(),
                    CreateDate = Convert.ToDateTime(cursor.Current.Values["CreateDate"]),
                    UpdateDate = Convert.ToDateTime(cursor.Current.Values["UpdateDate"])
                });
            }
            return lstCategories;
        }
    }
}
