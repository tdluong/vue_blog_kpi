﻿using Blog.Data;
using System.Threading.Tasks;

namespace Blog.service
{
    public interface IPostService
    {
        Task<DataResult> CreateSync(Post post);
        Task<DataResult> EditSync(Post post);
        Task<DataResult> GetDetailsSync(string id);
        Task<DataResult> GetDataSync();
        Task<DataResult> GetLastPostaSync();
        Task<DataResult> DeleteSync(string id);
        Task<DataResult> CreateRelationSync(Post post);
        Task<DataResult> DeleteRelationShipSync(Post post);
        Task<DataResult> GetDataByPostIdAsync(string postId);
    }
}
