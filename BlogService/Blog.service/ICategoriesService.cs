﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blog.Data;

namespace Blog.service
{
    public interface ICategoriesService
    {
        Task<DataResult> CreateSync(Categories categories);
        Task<DataResult> EditSync(Categories categories);
        Task<DataResult> GetDetails(string id);
        Task<DataResult> GetDataAsync();
        Task<DataResult> GetDataByPostIdAsync(string postId);
        Task<DataResult> DeleteSync(string id);
    }
}
