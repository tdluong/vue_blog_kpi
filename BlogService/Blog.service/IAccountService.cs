﻿using Blog.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blog.service
{
    public interface IAccountService
    {
        Task<DataResult> AddToRoleAsync(string email);
        Task<DataResult> GetUsersInRoleAsync();
        Task<DataResult> GetRoleByEmai(string email);
    }
}
