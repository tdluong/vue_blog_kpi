﻿using Blog.Data;
using Neo4j.Driver.V1;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blog.Repo
{
   public interface IRepository<T>
    {
        IDriver GetDriver();
        Task<IStatementResultCursor> GetAllAsync(string statement);
        T Get(long id);
        Task<IStatementResultCursor> InsertAsyncByCursor(string statement, object parameter);
        Task InsertAsync(string statement, object parameter);
        Task UpdateAsync(string statement, object parameter);

        Task DeleteAsync(string statement, object parameter);
        Task CreateRelationAsync(string statement, object parameter);
        Task DeleteRelationAsync(string statement, object parameter);
        Task SaveChanges();
        Task<IStatementResultCursor> GetRoleByUser(string statement, object parameter);
        Task UpdateTransactionAsync(ITransaction tx, string statement, object parameters);
        Task DeleteTransactionAsync(ITransaction tx, string statement, object parameters);
        Task CreateTransactionAsync(ITransaction tx, string statement, object parameters);

        void Dispose();
    }
}
